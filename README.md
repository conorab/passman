# passman

Passman is a simple password manager built for recent versions of G.N.U./Linux (requires the Bourne Again Shell; hence G.N.U.) which utilises G.P.G. to store and retrieve passwords from a given text file. Passman aims to be simple for two reasons: 1) to make it easy to audit/read the code yourself, and 2) to allow the resulting files to be easy to managed without it (the files storing passwords are nothing more than password provided at creation time, encrypted with G.P.G. 
